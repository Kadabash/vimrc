:set encoding=utf-8

:set expandtab
:set shiftwidth=4
:set softtabstop=4
:set autoindent
:set smartindent
filetype plugin indent on

:set number

syntax on

" Configure vim-syntastic:
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['python', 'pep8', 'mypy']
let g:syntastic_python_mypy_args = "--ignore-missing-imports"  " Don't warn on unannotated modules
let g:syntastic_python_python_exec = '/usr/bin/python3'

" Configure vim-youcompleteme:
let g:ycm_global_ycm_extra_conf = '/usr/lib/ycmd/ycm_extra_conf.py'
